dEQP-EGL.functional.sharing.gles2.multithread
dEQP-EGL.functional.query_context.simple.query_api
dEQP-EGL.functional.thread_cleanup
dEQP-EGL.functional.robustness.reset_context.fixed_function_pipeline.reset_status.index_buffer_out_of_bounds
dEQP-EGL.functional.multithread.pbuffer_single_window
dEQP-EGL.functional.multithread.pbuffer_single_window_context
dEQP-GLES31.functional.debug.negative_coverage.get_error.shader.link_compute_shader
# Disabled for https://bugs.freedesktop.org/show_bug.cgi?id=103746
dEQP-GLES31.functional.copy_image.non_compressed
# Disabled for https://bugs.freedesktop.org/show_bug.cgi?id=103746
dEQP-GLES31.functional.copy_image.compressed
# Disabled for https://bugs.freedesktop.org/show_bug.cgi?id=103746
dEQP-GLES31.functional.copy_image.mixed
#flaky
dEQP-GLES31.functional.debug.negative_coverage.callbacks.compute.exceed_atomic_counters_limit
