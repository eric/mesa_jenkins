dEQP-EGL.functional.buffer_age.no_preserve
dEQP-EGL.functional.sharing.gles2.multithread
dEQP-EGL.functional.query_context.simple.query_api
dEQP-EGL.functional.thread_cleanup
dEQP-EGL.functional.robustness.reset_context.fixed_function_pipeline.reset_status.index_buffer_out_of_bounds
dEQP-EGL.functional.multithread.pbuffer_single_window
dEQP-EGL.functional.multithread.pbuffer_single_window_context

dEQP-GLES31.functional.synchronization.inter_call.without_memory_barrier.ssbo_atomic_counter_mixed_dispatch_100_calls_1k_invocations
# Disabled for https://bugs.freedesktop.org/show_bug.cgi?id=103746
dEQP-GLES31.functional.copy_image.non_compressed
# Disabled for https://bugs.freedesktop.org/show_bug.cgi?id=103746
dEQP-GLES31.functional.copy_image.compressed
# Disabled for https://bugs.freedesktop.org/show_bug.cgi?id=103746
dEQP-GLES31.functional.copy_image.mixed

# GPU hang on ICL
dEQP-GLES31.functional.compute.basic.image_atomic_op_local_size_1
dEQP-GLES31.functional.compute.basic.image_atomic_op_local_size_8
dEQP-GLES31.functional.image_load_store.2d.qualifiers
dEQP-GLES31.functional.image_load_store.2d_array.qualifiers
dEQP-GLES31.functional.image_load_store.3d.qualifiers
dEQP-GLES31.functional.image_load_store.cube.qualifiers
dEQP-GLES31.functional.shaders.multisample_interpolation.interpolate_at_sample
dEQP-GLES31.functional.synchronization.inter_invocation
dEQP-GLES31.functional.ssbo.atomic.compswap
dEQP-GLES31.functional.tessellation.shader_input_output.barrier

# flaky on ICL
dEQP-GLES31.functional.sample_shading.min_sample_shading.multisample_renderbuffer_samples_8_color
dEQP-GLES31.functional.tessellation.invariance.primitive_set.quads_fractional_even_spacing_ccw_point_mode

#flaky
dEQP-GLES31.functional.debug.negative_coverage.callbacks.compute.exceed_atomic_counters_limit
dEQP-GLES31.functional.shaders.opaque_type_indexing.sampler.uniform.compute.isampler2d
dEQP-GLES31.functional.shaders.opaque_type_indexing.sampler.uniform.compute.isampler2darray

# flaky on ICL
dEQP-GLES3.functional.texture.mipmap.2d
dEQP-GLES3.functional.texture.mipmap.3d
dEQP-GLES3.functional.texture.mipmap.cube
